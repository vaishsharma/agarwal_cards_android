package apps.com.agarwalcards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Admin on 19-Jul-16.
 */
public class PerticularAdapter extends BaseAdapter {

    ArrayList<Perticular> arrPerticulars;
    Context context;
    LayoutInflater mInflater;


    public PerticularAdapter(ArrayList<Perticular> arrPerticulars, Context context) {

        this.arrPerticulars = arrPerticulars;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrPerticulars.size();
    }

    @Override
    public Object getItem(int position) {
        return arrPerticulars.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.single_perticular_item, null);
            holder.txtSrNo = (TextView) convertView.findViewById(R.id.txtSrNo);
            holder.txtVendorCode = (TextView) convertView.findViewById(R.id.txtVendorCode);
            holder.txtProductCode = (TextView) convertView.findViewById(R.id.txtProductCode);
            holder.txtDescription = (TextView) convertView.findViewById(R.id.txtDescription);
            holder.txtQuantity = (TextView) convertView.findViewById(R.id.txtQuantity);
            holder.txtRate = (TextView) convertView.findViewById(R.id.txtRate);
            holder.txtAmount = (TextView) convertView.findViewById(R.id.txtAmount);
            convertView.setTag(holder);
        }
        else
        {
            holder = (Holder) convertView.getTag();
        }
        holder.txtSrNo.setText(arrPerticulars.get(position).getSrNo());
        holder.txtVendorCode.setText(arrPerticulars.get(position).getVendorCode());
        holder.txtProductCode.setText(arrPerticulars.get(position).getProductCode());
        holder.txtDescription.setText(arrPerticulars.get(position).getDescription());
        holder.txtQuantity.setText(arrPerticulars.get(position).getQuantity());
        holder.txtRate.setText(arrPerticulars.get(position).getRate());
        holder.txtAmount.setText(arrPerticulars.get(position).getAmount());
        return convertView;
    }

    public class Holder
    {
        TextView txtVendorCode,txtProductCode,txtDescription,txtQuantity,txtRate,txtAmount,txtSrNo;
    }

}
