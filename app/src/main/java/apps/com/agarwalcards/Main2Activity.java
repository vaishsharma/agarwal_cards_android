package apps.com.agarwalcards;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Main2Activity extends AppCompatActivity {

    public static final MediaType FORM_DATA_TYPE
            = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    //URL derived from form URL
    public static final String URL = "https://docs.google.com/forms/d/e/1FAIpQLScVXcxdzVbd4H5QLoyexKCcMlkilVdQP3OuDtMCHnyjI-jDoQ/formResponse";
    //input element ids found from the live form page
    public static final String NAME_KEY = "entry.1282006955";
    public static final String ADDRESS_KEY = "entry.1844527915";
    public static final String MOBILE_NO_KEY = "entry.616837216";
    public static final String ALTERNATE_NO_KEY = "entry.2135100867";
    public static final String EMAIL_ID_KEY = "entry.453814365";
    public static final String EVENT_TYPE_KEY = "entry.1841548099";
    public static final String EVENT_DATE_KEY = "entry.1589035582";
    public static final String KEY_PERSONS_NAME_KEY = "entry.1554339352";
    public static final String CONTACT_NO_KEY = "entry.819894189";
    public static final String DATE_OF_BIRTH_KEY = "entry.2008250653";
    public static final String ORDER_TYPE_KEY = "entry.884848460";
    public static final String ORDER_ID_KEY = "entry.1908947673";
    public static final String ORDER_DATE_KEY = "entry.696408988";
    public static final String ORDER_STATUS_KEY = "entry.808757626";
    public static final String ASSIGNED_TO_KEY = "entry.846805674";
    public static final String VENDOR_CODE_KEY = "entry.790128926";
    public static final String PRODUCT_CODE_KEY = "entry.1220667137";
    public static final String DESCRIPTION_KEY = "entry.994808915";
    public static final String QUANTITY_KEY = "entry.578830145";
    public static final String RATE_KEY = "entry.814359688";
    public static final String AMOUNT_KEY = "entry.429430376";
    public static final String TOTAL_AMOUNT_KEY = "entry.1593671859";
    public static final String VAT_KEY = "entry.572041725";
    public static final String GRAND_TOTAL_KEY = "entry.1469875159";
    public static final String ADVANCE_KEY = "entry.1202110572";
    public static final String BALANCE_KEY = "entry.1078684184";
    public static final String SETTLEMENT_KEY = "entry.469753944";
    private Context context;
    private EditText nameEditText, addressEditText, mobileNoEditText, alternateNoEditText, emailIdEditText, keyPersonsNameEditText, contactNoEditText, orderIdEditText, assignedToEditText;
    private EditText vendorCodeEditText, productCodeEditText, descriptionEditText, quantityEditText, rateEditText, vatEditText, grandTotalEditText, advanceEditText, balanceEditText, setelmentEditText;
    private TextView txtEventDate, txtBirthDate, txtOrderDate, txtTotalAmount;
    private Spinner eventTypeSpinner, orderTypeSpinner, orderStatusSpinner;
    private DatePickerDialog dobPickerDialog;
    private SimpleDateFormat dateFormatter;
    private String strDate;
    private String strEventType;
    private String strOrderType;
    private String strOrderStatus;
    private float strAmount, totalAmount;
    private int count = 0;
    private List<String> eventTypeList, orderTypeList, orderStatusList;
    private LinearLayout orderDetailsLayout;
    private Button sendButton, btnAdd;
    private ArrayList<Perticular> listPerticulars;
    private ListView listView;
    private PerticularAdapter perticularAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        context = this;
        sendButton = (Button) findViewById(R.id.sendButton);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        mobileNoEditText = (EditText) findViewById(R.id.mobileNoEditText);
        alternateNoEditText = (EditText) findViewById(R.id.alternateNoEditText);
        emailIdEditText = (EditText) findViewById(R.id.emailIdEditText);
        keyPersonsNameEditText = (EditText) findViewById(R.id.keyPersonNameEditText);
        contactNoEditText = (EditText) findViewById(R.id.contactNoEditText);
        orderIdEditText = (EditText) findViewById(R.id.orderIdEditText);
        assignedToEditText = (EditText) findViewById(R.id.assignedToEditText);
        vatEditText = (EditText) findViewById(R.id.vatEditText);
        grandTotalEditText = (EditText) findViewById(R.id.grandTotalToEditText);
        advanceEditText = (EditText) findViewById(R.id.advanceEditText);
        balanceEditText = (EditText) findViewById(R.id.balanceEditText);
        setelmentEditText = (EditText) findViewById(R.id.setelmentEditText);
        txtEventDate = (TextView) findViewById(R.id.txtEventDate);
        txtBirthDate = (TextView) findViewById(R.id.txtBirthDate);
        txtOrderDate = (TextView) findViewById(R.id.txtOrderDate);
        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        eventTypeSpinner = (Spinner) findViewById(R.id.spnEventType);
        orderTypeSpinner = (Spinner) findViewById(R.id.spnOrderType);
        orderStatusSpinner = (Spinner) findViewById(R.id.spnOrderStatus);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        eventTypeList = new ArrayList<String>();
        orderTypeList = new ArrayList<String>();
        orderStatusList = new ArrayList<String>();
        orderDetailsLayout = (LinearLayout) findViewById(R.id.orderDetailsLayout);
        listPerticulars = new ArrayList<Perticular>();
        listView = (ListView) findViewById(R.id.listPerticulars);

        //getSupportActionBar().setTitle("Agarwal Cards");

        // Spinner Drop down elements
        eventTypeList.add("Birthday");
        eventTypeList.add("Anniversary");
        eventTypeList.add("Marriage");

        orderTypeList.add("Enquiry");
        orderTypeList.add("Confirmation");

        orderStatusList.add("Goods Confirmed");
        orderStatusList.add("Goods Ordered");
        orderStatusList.add("Goods Received");
        orderStatusList.add("Matter Received");
        orderStatusList.add("DTP In Progress");
        orderStatusList.add("DTP Ready");
        orderStatusList.add("Printing In Process");
        orderStatusList.add("Print Ready");
        orderStatusList.add("Assembly In Process");
        orderStatusList.add("Goods Ready");

        // Creating adapter for spinner
        ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, eventTypeList);
        ArrayAdapter<String> orderTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, orderTypeList);
        ArrayAdapter<String> orderStatusAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, orderStatusList);

        // Drop down layout style - list view with radio button
        eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        eventTypeSpinner.setAdapter(eventTypeAdapter);
        orderTypeSpinner.setAdapter(orderTypeAdapter);
        orderStatusSpinner.setAdapter(orderStatusAdapter);

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strEventType = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        orderTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strOrderType = parent.getItemAtPosition(position).toString();
                if (strOrderType.equals("Confirmation")) {
                    orderDetailsLayout.setVisibility(View.VISIBLE);
                } else {
                    orderDetailsLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        orderStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strOrderStatus = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(txtBirthDate);
            }
        });

        txtEventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(txtEventDate);
            }
        });

        txtOrderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(txtOrderDate);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog alertDialog;
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Main2Activity.this);
                dialogBuilder.setTitle("Add Perticulars");
                LayoutInflater inflater = Main2Activity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.add_product_layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        vendorCodeEditText = (EditText) dialogView.findViewById(R.id.vendorCodeEditText);
                        productCodeEditText = (EditText) dialogView.findViewById(R.id.productCodeEditText);
                        descriptionEditText = (EditText) dialogView.findViewById(R.id.descriptionEditText);
                        quantityEditText = (EditText) dialogView.findViewById(R.id.quantityEditText);
                        rateEditText = (EditText) dialogView.findViewById(R.id.rateEditText);
                        strAmount = Integer.valueOf(quantityEditText.getText().toString()) * Float.parseFloat(rateEditText.getText().toString());
                        int i = listPerticulars.size() + 1;
                        Perticular perticular = new Perticular();
                        perticular.setSrNo(String.valueOf(i));
                        perticular.setVendorCode(vendorCodeEditText.getText().toString());
                        perticular.setProductCode(productCodeEditText.getText().toString());
                        perticular.setDescription(descriptionEditText.getText().toString());
                        perticular.setQuantity(quantityEditText.getText().toString());
                        perticular.setRate(rateEditText.getText().toString());
                        perticular.setAmount(String.format("%.2f", strAmount));
                        listPerticulars.add(perticular);
                        totalAmount += Float.valueOf(listPerticulars.get(listPerticulars.size() - 1).getAmount());
                        txtTotalAmount.setText(String.format("%.2f", totalAmount));
                        perticularAdapter = new PerticularAdapter(listPerticulars, Main2Activity.this);
                        listView.setAdapter(perticularAdapter);
                        perticularAdapter.notifyDataSetChanged();
                    }
                });
                alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Make sure all the fields are filled with values
                /*if(TextUtils.isEmpty(nameEditText.getText().toString()) ||
                        TextUtils.isEmpty(addressEditText.getText().toString()) ||
                        TextUtils.isEmpty(mobileNoEditText.getText().toString()))
                {
                    Toast.makeText(context,"All fields are mandatory.",Toast.LENGTH_LONG).show();
                    return;
                }*/
                if(validate()) {


                    if (strOrderType.equals("Enquiry")) {
                        PostEnquiryTask postEnquiryTask = new PostEnquiryTask();

                        //execute asynctask
                        postEnquiryTask.execute(URL, nameEditText.getText().toString(),
                                addressEditText.getText().toString(),
                                mobileNoEditText.getText().toString(),
                                alternateNoEditText.getText().toString(),
                                emailIdEditText.getText().toString(),
                                strEventType, txtEventDate.getText().toString(),
                                keyPersonsNameEditText.getText().toString(),
                                contactNoEditText.getText().toString(),
                                txtBirthDate.getText().toString(), strOrderType
                        );
                    } else {

                        //Create an object for PostDataTask AsyncTask
                        PostDataTask postDataTask = new PostDataTask();

                        //execute asynctask
                        postDataTask.execute(URL, nameEditText.getText().toString(),
                                addressEditText.getText().toString(),
                                mobileNoEditText.getText().toString(),
                                alternateNoEditText.getText().toString(),
                                emailIdEditText.getText().toString(),
                                strEventType, txtEventDate.getText().toString(),
                                keyPersonsNameEditText.getText().toString(),
                                contactNoEditText.getText().toString(),
                                txtBirthDate.getText().toString(), strOrderType,
                                orderIdEditText.getText().toString(),
                                txtOrderDate.getText().toString(),
                                strOrderStatus,
                                assignedToEditText.getText().toString(),
                                String.valueOf(totalAmount),
                                vatEditText.getText().toString(),
                                grandTotalEditText.getText().toString(),
                                advanceEditText.getText().toString(),
                                balanceEditText.getText().toString(),
                                setelmentEditText.getText().toString()

                        );

                        PostPerticularsTask postPerticularsTask = new PostPerticularsTask();

                /*for(int i=0;i<listPerticulars.size();i++)
                {*/
                        if (listPerticulars.size() > 0) {
                            postPerticularsTask.execute(URL,
                                    mobileNoEditText.getText().toString(),
                                    orderIdEditText.getText().toString(),
                                    listPerticulars.get(0).getVendorCode(),
                                    listPerticulars.get(0).getProductCode(),
                                    listPerticulars.get(0).getDescription(),
                                    listPerticulars.get(0).getQuantity(),
                                    listPerticulars.get(0).getRate(),
                                    listPerticulars.get(0).getAmount()
                            );
                        }
                        //}
                    }
                }
            }
        });
        /*for(int j=0;j<listPerticulars.size();j++)
        {
            totalAmount += Integer.valueOf(listPerticulars.get(j).getAmount());
            //txtTotalAmount.setText(String.valueOf(totalAmount));
        }
        txtTotalAmount.setText(String.valueOf(totalAmount));*/

    }

    private void setDate(final TextView txt) {
        Calendar newCalendar = Calendar.getInstance();
        dobPickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                txt.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        dobPickerDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class PostDataTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... contactData) {
            Boolean result = true;
            String url = contactData[0];
            String name = contactData[1];
            String address = contactData[2];
            String mobileNumber = contactData[3];
            String alternateNumber = contactData[4];
            String emailId = contactData[5];
            String eventType = contactData[6];
            String eventDate = contactData[7];
            String keyPersonsName = contactData[8];
            String contactNumber = contactData[9];
            String birthDate = contactData[10];
            String orderType = contactData[11];
            String orderID = contactData[12];
            String orderDate = contactData[13];
            String orderStatus = contactData[14];
            String assignedTo = contactData[15];
            String totalAmount = contactData[16];
            String vat = contactData[17];
            String grandTotal = contactData[18];
            String advance = contactData[19];
            String balance = contactData[20];
            String settlement = contactData[21];

            String postBody = "";

            try {
                //all values must be URL encoded to make sure that special characters like & | ",etc.
                //do not cause problems
                postBody = NAME_KEY + "=" + URLEncoder.encode(name, "UTF-8") +
                        "&" + ADDRESS_KEY + "=" + URLEncoder.encode(address, "UTF-8") +
                        "&" + MOBILE_NO_KEY + "=" + URLEncoder.encode(mobileNumber, "UTF-8") +
                        "&" + ALTERNATE_NO_KEY + "=" + URLEncoder.encode(alternateNumber, "UTF-8") +
                        "&" + EMAIL_ID_KEY + "=" + URLEncoder.encode(emailId, "UTF-8") +
                        "&" + EVENT_TYPE_KEY + "=" + URLEncoder.encode(eventType, "UTF-8") +
                        "&" + EVENT_DATE_KEY + "=" + URLEncoder.encode(eventDate, "UTF-8") +
                        "&" + KEY_PERSONS_NAME_KEY + "=" + URLEncoder.encode(keyPersonsName, "UTF-8") +
                        "&" + CONTACT_NO_KEY + "=" + URLEncoder.encode(contactNumber, "UTF-8") +
                        "&" + DATE_OF_BIRTH_KEY + "=" + URLEncoder.encode(birthDate, "UTF-8") +
                        "&" + ORDER_TYPE_KEY + "=" + URLEncoder.encode(orderType, "UTF-8") +
                        "&" + ORDER_ID_KEY + "=" + URLEncoder.encode(orderID, "UTF-8") +
                        "&" + ORDER_DATE_KEY + "=" + URLEncoder.encode(orderDate, "UTF-8") +
                        "&" + ORDER_STATUS_KEY + "=" + URLEncoder.encode(orderStatus, "UTF-8") +
                        "&" + ASSIGNED_TO_KEY + "=" + URLEncoder.encode(assignedTo, "UTF-8") +
                        "&" + TOTAL_AMOUNT_KEY + "=" + URLEncoder.encode(totalAmount, "UTF-8") +
                        "&" + VAT_KEY + "=" + URLEncoder.encode(vat, "UTF-8") +
                        "&" + GRAND_TOTAL_KEY + "=" + URLEncoder.encode(grandTotal, "UTF-8") +
                        "&" + ADVANCE_KEY + "=" + URLEncoder.encode(advance, "UTF-8") +
                        "&" + BALANCE_KEY + "=" + URLEncoder.encode(balance, "UTF-8") +
                        "&" + SETTLEMENT_KEY + "=" + URLEncoder.encode(settlement, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                result = false;
            }

            /*//If you want to use HttpRequest class from http://stackoverflow.com/a/2253280/1261816
            try {
			HttpRequest httpRequest = new HttpRequest();
			httpRequest.sendPost(url, postBody);
		}catch (Exception exception){
			result = false;
		}*/

            try {
                //Create OkHttpClient for sending request
                OkHttpClient client = new OkHttpClient();
                //Create the request body with the help of Media Type
                RequestBody body = RequestBody.create(FORM_DATA_TYPE, postBody);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                //Send the request
                Response response = client.newCall(request).execute();
                if (response.code() == 200) {
                    result = true;
                }
            } catch (IOException exception) {
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            //Print Success or failure message accordingly
            Toast.makeText(context, result ? "Message successfully sent!" : "There was some error in sending message. Please try again after some time.", Toast.LENGTH_LONG).show();
        }

    }

    private class PostPerticularsTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... contactData) {
            Boolean result = true;
            String url = contactData[0];
            String mobileNumber = contactData[1];
            String orderID = contactData[2];
            String vendorCode = contactData[3];
            String productCode = contactData[4];
            String description = contactData[5];
            String quantity = contactData[6];
            String rate = contactData[7];
            String amount = contactData[8];
            String postBody = "";

            try {
                //all values must be URL encoded to make sure that special characters like & | ",etc.
                //do not cause problems
                postBody = MOBILE_NO_KEY + "=" + URLEncoder.encode(mobileNumber, "UTF-8") +
                        "&" + ORDER_ID_KEY + "=" + URLEncoder.encode(orderID, "UTF-8") +
                        "&" + VENDOR_CODE_KEY + "=" + URLEncoder.encode(vendorCode, "UTF-8") +
                        "&" + PRODUCT_CODE_KEY + "=" + URLEncoder.encode(productCode, "UTF-8") +
                        "&" + DESCRIPTION_KEY + "=" + URLEncoder.encode(description, "UTF-8") +
                        "&" + QUANTITY_KEY + "=" + URLEncoder.encode(quantity, "UTF-8") +
                        "&" + RATE_KEY + "=" + URLEncoder.encode(rate, "UTF-8") +
                        "&" + AMOUNT_KEY + "=" + URLEncoder.encode(amount, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                result = false;
            }

            /*//If you want to use HttpRequest class from http://stackoverflow.com/a/2253280/1261816
            try {
			HttpRequest httpRequest = new HttpRequest();
			httpRequest.sendPost(url, postBody);
		}catch (Exception exception){
			result = false;
		}*/

            try {
                //Create OkHttpClient for sending request
                OkHttpClient client = new OkHttpClient();
                //Create the request body with the help of Media Type
                RequestBody body = RequestBody.create(FORM_DATA_TYPE, postBody);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                //Send the request
                Response response = client.newCall(request).execute();
                if (response.code() == 200) {
                    result = true;
                }
            } catch (IOException exception) {
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            count++;
            if (count < listPerticulars.size()) {
                new PostPerticularsTask().execute(URL,
                        mobileNoEditText.getText().toString(),
                        orderIdEditText.getText().toString(),
                        listPerticulars.get(count).getVendorCode(),
                        listPerticulars.get(count).getProductCode(),
                        listPerticulars.get(count).getDescription(),
                        listPerticulars.get(count).getQuantity(),
                        listPerticulars.get(count).getRate(),
                        listPerticulars.get(count).getAmount());
            }
            if (count == listPerticulars.size()) {
                count = 0;
                clear();
            }

            //Print Success or failure message accordingly
            //Toast.makeText(context,result?"Message successfully sent!":"There was some error in sending message. Please try again after some time.",Toast.LENGTH_LONG).show();
        }

    }

    private class PostEnquiryTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... contactData) {
            Boolean result = true;
            String url = contactData[0];
            String name = contactData[1];
            String address = contactData[2];
            String mobileNumber = contactData[3];
            String alternateNumber = contactData[4];
            String emailId = contactData[5];
            String eventType = contactData[6];
            String eventDate = contactData[7];
            String keyPersonsName = contactData[8];
            String contactNumber = contactData[9];
            String birthDate = contactData[10];
            String orderType = contactData[11];
            String postBody = "";

            try {
                //all values must be URL encoded to make sure that special characters like & | ",etc.
                //do not cause problems
                postBody = NAME_KEY + "=" + URLEncoder.encode(name, "UTF-8") +
                        "&" + ADDRESS_KEY + "=" + URLEncoder.encode(address, "UTF-8") +
                        "&" + MOBILE_NO_KEY + "=" + URLEncoder.encode(mobileNumber, "UTF-8") +
                        "&" + ALTERNATE_NO_KEY + "=" + URLEncoder.encode(alternateNumber, "UTF-8") +
                        "&" + EMAIL_ID_KEY + "=" + URLEncoder.encode(emailId, "UTF-8") +
                        "&" + EVENT_TYPE_KEY + "=" + URLEncoder.encode(eventType, "UTF-8") +
                        "&" + EVENT_DATE_KEY + "=" + URLEncoder.encode(eventDate, "UTF-8") +
                        "&" + KEY_PERSONS_NAME_KEY + "=" + URLEncoder.encode(keyPersonsName, "UTF-8") +
                        "&" + CONTACT_NO_KEY + "=" + URLEncoder.encode(contactNumber, "UTF-8") +
                        "&" + DATE_OF_BIRTH_KEY + "=" + URLEncoder.encode(birthDate, "UTF-8") +
                        "&" + ORDER_TYPE_KEY + "=" + URLEncoder.encode(orderType, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                result = false;
            }

            /*//If you want to use HttpRequest class from http://stackoverflow.com/a/2253280/1261816
            try {
			HttpRequest httpRequest = new HttpRequest();
			httpRequest.sendPost(url, postBody);
		}catch (Exception exception){
			result = false;
		}*/

            try {
                //Create OkHttpClient for sending request
                OkHttpClient client = new OkHttpClient();
                //Create the request body with the help of Media Type
                RequestBody body = RequestBody.create(FORM_DATA_TYPE, postBody);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                //Send the request
                Response response = client.newCall(request).execute();
                if (response.code() == 200) {
                    result = true;
                }
            } catch (IOException exception) {
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            //Print Success or failure message accordingly
            Toast.makeText(context, result ? "Message successfully sent!" : "There was some error in sending message. Please try again after some time.", Toast.LENGTH_LONG).show();
            clear();
        }

    }

    public void clear()
    {
        nameEditText.setText("");
        addressEditText.setText("");
        mobileNoEditText.setText("");
        alternateNoEditText.setText("");
        emailIdEditText.setText("");
        keyPersonsNameEditText.setText("");
        contactNoEditText.setText("");
        orderIdEditText.setText("");
        assignedToEditText.setText("");
        vatEditText.setText("");
        grandTotalEditText.setText("");
        advanceEditText.setText("");
        balanceEditText.setText("");
        setelmentEditText.setText("");
        txtEventDate.setText("Event Date");
        txtBirthDate.setText("Date of Birth");
        txtOrderDate.setText("Order Date");
        txtTotalAmount.setText("");
        listPerticulars.clear();
        //if(listPerticulars.size()>0) {
            //perticularAdapter.notifyDataSetChanged();
        //}
        totalAmount=0;
    }

    public boolean validate() {
        boolean valid = true;
        String name = nameEditText.getText().toString();
        String mobileNumber = mobileNoEditText.getText().toString();
        String alternateNumber = alternateNoEditText.getText().toString();
        String email = emailIdEditText.getText().toString();
        String contactNumber= contactNoEditText.getText().toString();;
        String orderId=orderIdEditText.getText().toString();
        final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        if (name.isEmpty())
        {
            nameEditText.setError("Required");
            valid = false;
        }
        if (!EMAIL_ADDRESS_PATTERN.matcher(email).matches())
        {
            emailIdEditText.setError("Invalid Email ID");
            valid = false;
        }
        if (mobileNumber.length()>0 && mobileNumber.length()<10) {
            mobileNoEditText.setError("Invalid Mobile Number");
            valid = false;
        }
        if (alternateNumber.length()>0 && alternateNumber.length()<10) {
            alternateNoEditText.setError("Invalid Alternate Number");
            valid = false;
        }
        if (contactNumber.length()>0 && contactNumber.length()<10) {
            contactNoEditText.setError("Invalid Contact Number");
            valid = false;
        }
        if (strOrderType.equals("Confirmation")) {
            if (orderId.isEmpty()) {
                orderIdEditText.setError("Required");
                valid = false;
            }
        }
        return valid;
    }
}
